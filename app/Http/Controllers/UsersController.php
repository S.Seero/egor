<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function import()
    {
        Excel::import(new UsersImport, request()->file('file'));

        return redirect('/')->with('success', 'Horosho');
    }

    public function importExportView()
    {
        return view('import');
    }
}
